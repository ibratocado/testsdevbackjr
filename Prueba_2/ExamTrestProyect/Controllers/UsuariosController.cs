﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ExamTrestProyect.Models;
using ExamTrestProyect.Services;
using ExamTrestProyect.Models.View;
using System.Text;
using Microsoft.Extensions.Primitives;

namespace ExamTrestProyect.Controllers
{
    public class UsuariosController : Controller
    {
        private readonly IUsuariosService _serviceUser;

        public UsuariosController(IUsuariosService serviceUser)
        {
            _serviceUser = serviceUser;
        }

        // GET: Usuarios
        public async Task<IActionResult> Index(int? id)
        {
            return View(await _serviceUser.GetTen(id));
        }

        // GET: Usuarios/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuario = await _serviceUser.GetOne(id);
            if (usuario == null)
            {
                return NotFound();
            }

            return View(usuario);
        }

        // GET: Usuarios/Create
        public IActionResult Create()
        {
            return View();
        }

        public FileResult Archivo()
        {
            var sb = _serviceUser.Report();
            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "Personas.csv");
        }



        // POST: Usuarios/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,Login,Name,Paterno,Materno,Sueldo")] UserCreate usuario)
        {
            if (ModelState.IsValid)
            {
                await _serviceUser.Create(usuario); 
                return RedirectToAction("Index",new {id = -1});
            }
            return View(usuario);
        }

        // GET: Usuarios/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var temp = await  _serviceUser.GetOne(id);

            UserUpdate user = new UserUpdate
            {
                UserId = temp.UserId,
                Sueldo = temp.Empleados.FirstOrDefault().Sueldo,
                Name = temp.Name,
                Paterno = temp.Paterno,
                Materno = temp.Materno
            };

            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UserId,Sueldo")] UserUpdate usuario)
        {
            if (id != usuario.UserId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _serviceUser.Update(usuario);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UsuarioExists(usuario.UserId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details",new {id = usuario.UserId});
            }
            return View(usuario);
        }


        private bool UsuarioExists(int id)
        {
           return _serviceUser.Exist(id);
        }
    }
}
