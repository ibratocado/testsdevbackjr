﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ExamTrestProyect.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
            Empleados = new HashSet<Empleado>();
        }

        public int UserId { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }
    }
}
