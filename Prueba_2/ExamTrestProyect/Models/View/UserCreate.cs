﻿using ExamTrestProyect.Validations;
using System;
using System.ComponentModel.DataAnnotations;

namespace ExamTrestProyect.Models.View
{
    public class UserCreate
    {
        [Required]
        [ValidationLogin]
        public string Login { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Paterno { get; set; }
        [Required]
        public string Materno { get; set; }
        [Required]
        [Range(1200,100000)]
        public double? Sueldo { get; set; }
    }
}
