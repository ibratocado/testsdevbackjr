﻿using System.ComponentModel.DataAnnotations;

namespace ExamTrestProyect.Models.View
{
    public class UserUpdate
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        [Required]
        [Range(1200, 100000)]
        public double? Sueldo { get; set; }
    }
}
