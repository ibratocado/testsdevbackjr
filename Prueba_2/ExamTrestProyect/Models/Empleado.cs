﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ExamTrestProyect.Models
{
    public partial class Empleado
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public double? Sueldo { get; set; }
        public DateTime? FechaIngreso { get; set; }

        public virtual Usuario User { get; set; }
    }
}
