﻿using ExamTrestProyect.Models;
using ExamTrestProyect.Models.View;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamTrestProyect.Services
{
    public interface IUsuariosService
    {
        Task<List<Usuario>> GetTen(int? id);
        Task<Usuario> GetOne(int id);
        Task<int> Create(UserCreate model);
        Task<int> Update(UserUpdate model);
        bool Exist(int id);
        string Report();

    }
}
