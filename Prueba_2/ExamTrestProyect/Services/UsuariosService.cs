﻿using ExamTrestProyect.Models;
using ExamTrestProyect.Models.View;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTrestProyect.Services
{
    public class UsuariosService : IUsuariosService
    {
        private TestBaseContext _context;

        public UsuariosService(TestBaseContext context)
        {
            _context = context;
        }

        public Task<int> Create(UserCreate model)
        {
            Usuario usuario = new Usuario
            {
                Login = model.Login,
                Name = model.Name,
                Paterno = model.Paterno,
                Materno = model.Materno,
            };

            _context.Add(usuario);
            _context.SaveChanges();
            

            Empleado empleado = new Empleado
            {
                UserId = usuario.UserId,
                Sueldo = model.Sueldo,
                FechaIngreso = DateTime.Now
            };
            _context.Add(empleado);
            

            return _context.SaveChangesAsync();
        }

        public Task<Usuario> GetOne(int id)
        {
            return _context.Usuarios.Include(i=>i.Empleados).FirstOrDefaultAsync(i=>i.UserId == id);
        }

        public Task<List<Usuario>> GetTen(int? id)
        {
            if(id == null)
                return _context.Usuarios.Include(i => i.Empleados).Take(10).ToListAsync();
            else
                if(id == -1)
                {
                    var list = _context.Usuarios.Include(i => i.Empleados);
                    var num = list.Count() - 11;
                    return list.Where(i=>i.UserId>=num).ToListAsync();
                }
            else
                return _context.Usuarios.Include(i => i.Empleados).Where(i=>i.UserId>=id).Take(10).ToListAsync();

        }

        public string Report()
        {
            var list = _context.Usuarios.Include(i => i.Empleados).ToList();
            StringBuilder sb = new StringBuilder();
            foreach (var item in list)
            {
                //aqui se agrega la propiedad y una coma.
                if (item.Empleados.Count != 0)
                {
                    sb.Append(item.UserId + " , "
                   + item.Name + " " + item.Paterno + " " + item.Materno + " , "
                   + item.Empleados.FirstOrDefault().Sueldo + " , "
                   + item.Empleados.FirstOrDefault().FechaIngreso);
                }
                //nueva linea
                sb.Append("\r\n");

            }
            return sb.ToString();
        }

        public Task<int> Update(UserUpdate model)
        {
            var temp = _context.Empleados.Find(model.UserId);
            temp.Sueldo = model.Sueldo;
            _context.Update(temp);
            return _context.SaveChangesAsync();
           
        }
        public bool Exist(int id)
        {
            return _context.Usuarios.Any(e => e.UserId == id);
        }
    }
}
