﻿using System.ComponentModel.DataAnnotations;

namespace ExamTrestProyect.Validations
{
    public class ValidationLogin : ValidationAttribute
    {
        public string GetErrorMessage() => $"No valido Formato User#";
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            
            if (value != null || !string.IsNullOrEmpty(value.ToString()))
            {
                var primera = value.ToString()[0].ToString() 
                    + value.ToString()[1].ToString() 
                    + value.ToString()[2].ToString() 
                    + value.ToString()[3].ToString();
                if (primera.Equals("User"))
                    return new ValidationResult(GetErrorMessage());
        
                for (int i = 4; i < value.ToString().Length; i++)
                        if( !(value.ToString()[i] >=49 && value.ToString()[i] < 57))
                            return new ValidationResult(GetErrorMessage());
            }
            return ValidationResult.Success;
        }
    }
}
