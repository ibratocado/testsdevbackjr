drop database TestBase;
create database TestBase;
use TestBase


go
if OBJECT_ID('usuarios') is not null 
	drop table usuarios;
go
create table usuarios(
	userId int identity primary key,
	login varchar(100) not null,
	Name varchar(100) not null,
	Paterno varchar(100) not null,
	Materno varchar(100) not null);


if OBJECT_ID('empleados') is not null 
	drop table empleados;
go
create table empleados (
	Id int primary key identity,
	userId int,
	Sueldo float,
	FechaIngreso date,
	constraint fk_user_empleado foreign key (userId) 
	references dbo.usuarios (userId) on update cascade on delete cascade);
